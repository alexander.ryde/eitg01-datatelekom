package lab2pack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

//Denna klass �rver lab2pack.Global s� att man kan anv�nda time och signalnamnen utan punktnotation


public class MainSimulationUppg1f extends Global{

    public static void main(String[] args) throws IOException {

		//Signallistan startas och actSignal deklareras. actSignal �r den senast utplockade signalen i huvudloopen nedan.

		Signal actSignal;
		new SignalList();

		//H�r nedan skapas de processinstanser som beh�vs och parametrar i dem ges v�rden.
		//Uppgift 1.f - En dispatcher (generator) som �r direktkopplat till fem betj�ningssystem.
		//                     |--> [   ]-o--->
		//                     |--> [   ]-o--->
		// ---> [Dispatcher] --+--> [   ]-o--->
		//                     |--> [   ]-o--->
		//                     |--> [   ]-o--->
		//
		//Skapa 5 system till en samling
		ArrayList<QueuingSystem> allSystems = new ArrayList<>();
		for (int i = 1; i <= 5; i++) {
			QueuingSystem q = new QueuingSystem();
			q.sendTo = null;
			allSystems.add(q);
		}



    	GenDispatch dispatcher = new GenDispatch();
//    	dispatcher.lambda = 9; //Generator ska generera nio kunder per sekund
//		dispatcher.lambda = 8; //Uppgift 1.e
		dispatcher.lambda = 45; //Uppgift 1.f
		dispatcher.sendTo = null; //De genererade kunderna ska skickas till ett av flera k�system
		dispatcher.allSystems = allSystems; //Uppgift 1.f. Dispatchern beh�ver n� systemens k�storlek
		System.out.println("Dispatchern kan prioritera mellan och f�rdela till kunderna p� olika s�tt.\n\nV�lj prioritet:\t[1] Slump\t[2] Sekvens\t[3] Mest-ledig");
		Scanner input = new Scanner(System.in);
		String choice = input.nextLine();
		dispatcher.sendToAlgoritm = SendToAlgoritm.SLUMP;
		if (choice.equals("2")) dispatcher.sendToAlgoritm = SendToAlgoritm.SEKVENS;
		else if (choice.equals("3")) dispatcher.sendToAlgoritm = SendToAlgoritm.MEST_LEDIG;

    	//H�r nedan skickas de f�rsta signalerna f�r att simuleringen ska komma ig�ng.
    	SignalList.SendSignal(READY, dispatcher, time);
    	for (QueuingSystem q: allSystems) SignalList.SendSignal(MEASURE, q, time);


    	// Detta �r simuleringsloopen:

    	while (time < 100000){
    		actSignal = SignalList.FetchSignal();
    		time = actSignal.arrivalTime;
    		actSignal.destination.TreatSignal(actSignal);
		}

    	//Slutligen skrivs resultatet av simuleringen ut nedan:

    	System.out.println("Medelantal kunder i k�systemen (F�rdelningsprioritet: " + dispatcher.sendToAlgoritm + ")");
    	double accumulatedAverage = 0;
    	for (QueuingSystem q: allSystems) {
    		double average = 1.0*q.accumulated/q.noMeasurements;
    		System.out.println("allSystems(" + allSystems.indexOf(q) + ") average: " + average);
			accumulatedAverage += average;
		}
		System.out.println("Average of averages  = " + accumulatedAverage/allSystems.size());

    }
}