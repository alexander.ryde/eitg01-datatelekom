package lab2pack;

import java.util.Collections;
import java.util.List;
import java.util.Random;

//Denna klass �rver lab2pack.Proc, det g�r att man kan anv�nda time och signalnamn utan punktnotation

/**
 * Som Gen och kan kopplas till fler �n en QS och skickar till dem enligt valbar prioritetsalgoritm
 * Om sendToAlgoritm inte s�tts s� blir det SendToAlgoritm.SEKVENS.
 */
class GenDispatch extends Proc{
	List<QueuingSystem> allSystems;
	SendToAlgoritm sendToAlgoritm = SendToAlgoritm.SEKVENS;
	private Integer sendToIndex = -1;

	//Slumptalsgeneratorn startas:
	Random slump = new Random();

	//Generatorn har tv� parametrar:
	public Proc sendTo;    //Anger till vilken process de genererade kunderna ska skickas
	public double lambda;  //Hur m�nga per sekund som ska generas

	//H�r nedan anger man vad som ska g�ras n�r en signal kommer
	public void TreatSignal(Signal x){
		switch (x.signalType){
			case READY:{
				//sendTo parametern styrs h�r beroende p� vald sendToAlgoritm i dispatchern
				switch (sendToAlgoritm) {
					case SLUMP: {
						//Slumpa listan med alla system och s�tt sen f�rsta elementet till sendTo
						Collections.shuffle(allSystems);
						sendTo = allSystems.get(0);
						break;
					}
					case SEKVENS: {
						//�ka sendToIndex med 1 men med mod s� att om den g�r �ver maxindex s� blir den 0 ist�llet
						sendToIndex = ++sendToIndex % allSystems.size();
						sendTo = allSystems.get(sendToIndex);
						break;
					}
					case MEST_LEDIG: {
						//G� igenom alla system och skicka efter�t till det som hade kortast k�
						Integer smallestQueueSize = Integer.MAX_VALUE;
						QueuingSystem qsWithSmallestQueue = allSystems.get(0); //Ge den icke-null-v�rde i on�dan
						for (QueuingSystem q: allSystems) {
							if (q.numberInQueue < smallestQueueSize) {
								//Systemet har kortare k� �n sets tidigare i listan (eller s� �r detta det f�rsta systemet)
								smallestQueueSize = q.numberInQueue;
								qsWithSmallestQueue = q;
							}
						}
						sendTo = qsWithSmallestQueue;
						break;
					}
				}
				SignalList.SendSignal(ARRIVAL, sendTo, time);
				SignalList.SendSignal(READY, this, time + (2.0/lambda)*slump.nextDouble());}
				break;
		}
	}
}