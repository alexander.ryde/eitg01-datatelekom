package lab2pack;

import java.io.*;

//Denna klass �rver lab2pack.Global s� att man kan anv�nda time och signalnamnen utan punktnotation


public class MainSimulation extends Global{

    public static void main(String[] args) throws IOException {

    	//Signallistan startas och actSignal deklareras. actSignal �r den senast utplockade signalen i huvudloopen nedan.

    	Signal actSignal;
    	new SignalList();

    	//H�r nedan skapas de processinstanser som beh�vs och parametrar i dem ges v�rden.
        //Uppgift 1.e - Tre grupper av k�+betj�nare i rad.
        // ---> [   ]-o--->   [   ]-o--->   [   ]-o--->
        QueuingSystem Q3 = new QueuingSystem();
        Q3.sendTo = null;
        QueuingSystem Q2 = new QueuingSystem();
        Q2.sendTo = Q3;
        QueuingSystem Q1 = new QueuingSystem();
        Q1.sendTo = Q2;


    	Gen Generator = new Gen();
//    	Generator.lambda = 9; //Generator ska generera nio kunder per sekund
    	Generator.lambda = 8; //Uppgift 1.e
    	Generator.sendTo = Q1; //De genererade kunderna ska skickas till k�systemet lab2pack.QueuingSystem

    	//H�r nedan skickas de f�rsta signalerna f�r att simuleringen ska komma ig�ng.

    	SignalList.SendSignal(READY, Generator, time);
    	SignalList.SendSignal(MEASURE, Q1, time);
    	SignalList.SendSignal(MEASURE, Q2, time);
    	SignalList.SendSignal(MEASURE, Q3, time);


    	// Detta �r simuleringsloopen:

    	while (time < 100000){
    		actSignal = SignalList.FetchSignal();
    		time = actSignal.arrivalTime;
    		actSignal.destination.TreatSignal(actSignal);
    	}

    	//Slutligen skrivs resultatet av simuleringen ut nedan:

    	System.out.println("Medelantal kunder i k�system Q1: " + 1.0*Q1.accumulated/Q1.noMeasurements);
    	System.out.println("                             Q2: " + 1.0*Q2.accumulated/Q2.noMeasurements);
    	System.out.println("                             Q3: " + 1.0*Q3.accumulated/Q3.noMeasurements);

    }
}