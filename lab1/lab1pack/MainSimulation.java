package lab1pack;

import java.io.*;


public class MainSimulation extends GlobalSimulation {

    public static void main(String[] args) throws IOException {
        Double totalAccumulatedAllTests = 0.0;
        Integer numTests = 10;
        for (int i = 1; i <= numTests; i++) {
            time = 0;
            Event actEvent;
            EventList myEventList = new EventList();
            State actState = new State(myEventList);
            myEventList.InsertEvent(ARRIVAL_A, 0);
            myEventList.InsertEvent(MEASURE, 0.1);  // tid mellan mätningar 0.1 s
            Double timeToRun = (0.1 * 1000); // göra 1000 mätningar där tiden mellan mätningar 0.1
            while (time < timeToRun) {
//            while (time < 200000) {
                //Hämta nästa event
                actEvent = myEventList.FetchEvent();
                //Sätt nuvarande tid till tiden för actEvent
                time = actEvent.eventTime;
                //Hantera eventet (kör olika metoder beroende på om ARRIVE, READY, MEASURE
                actState.TreatEvent(actEvent);
            }
            double meanNumCustomers = 1.0 * actState.accumulated / actState.noMeasurements;
            System.out.print("Test #" + i + ":\t\t\tMean number of customers: " + 1.0 * meanNumCustomers + "\t\t\t");
//            System.out.print("Test #" + i + ":\t\t\tMean number of customers: " + 1.0 * actState.accumulated / actState.noMeasurements + "\t\t\t");
            System.out.println(" Number of measurements done: " + actState.noMeasurements);
            totalAccumulatedAllTests += meanNumCustomers;
        }
        System.out.println("----------------------------------------------------------------------------------------------");
        System.out.println(numTests + " tests\t\t\t\t\t   Mean of means: " + totalAccumulatedAllTests / numTests);
    }
}