package lab1pack;

/**
 * Konstanter:<br>
 * ARRIVAL	= 1<br>
 * READY	= 2<br>
 * MEASURE	= 3<br>
 * <br>
 * Även:<br>
 * double time = 0
 */
public class GlobalSimulation{
	public static final int ARRIVAL_A = 1, READY_A = 2, MEASURE = 3, ARRIVAL_B = 4, READY_B = 5;
	public static double time = 0;
}
