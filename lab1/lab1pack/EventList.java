package lab1pack;

/**
 * Essentially a linked list.
 *
 * Events are sorted according to TimeOfEvent.
 * FetchEvent will always return earliest scheduled lab1pack.Event.
 */
public class EventList{

	private Event list, last;


	EventList(){
		list = new Event();
    	last = new Event();
    	list.next = last;
	}

	/**
	 * Insert event. List is automatically sorted by TimeOfEvent.
	 * @param type -
	 * @param TimeOfEvent -
	 */
	public void InsertEvent(int type, double TimeOfEvent){
 	Event dummy, predummy;
 	Event newEvent = new Event();
 	newEvent.eventType = type;
 	newEvent.eventTime = TimeOfEvent;
 	predummy = list;
 	dummy = list.next;
 	while ((dummy.eventTime < newEvent.eventTime) & (dummy != last)){
 		predummy = dummy;
 		dummy = dummy.next;
 	}
 	predummy.next = newEvent;
 	newEvent.next = dummy;
 }

	/**
	 * Retrieves next event in line, according to TimeOfEvent.
	 * @return Next event in line, according to TimeOfEvent.
	 */
	public Event FetchEvent(){
		Event dummy;
		//Detta är inte dummy = list då den första i länkade listan är tomt event som aldrig används.
		dummy = list.next;
		list.next = dummy.next;
		dummy.next = null;
		return dummy;
	}
}
