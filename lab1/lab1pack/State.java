package lab1pack;

import java.util.*;

class State extends GlobalSimulation {

    public int numberInQueueA = 0, numberInQueueB = 0, accumulated = 0, noMeasurements = 0;

    private EventList myEventList;

    Random slump = new Random();

    State(EventList x) {
        myEventList = x;
    }

    private void InsertEvent(int event, double timeOfEvent) {
        myEventList.InsertEvent(event, timeOfEvent);
    }


    public void TreatEvent(Event x) {
        switch (x.eventType) {
            case ARRIVAL_A:
                arrival_a();
                break;
            case ARRIVAL_B:
                arrival_b();
                break;
            case READY_A:
                ready_a();
                break;
            case READY_B:
                ready_b();
                break;
            case MEASURE:
                measure();
                break;
        }

    }

    // Slumpa tal mellan 0 och [dubbelt av medelvärdet].
    private double generateMean(double mean) {
        return 2 * mean * slump.nextDouble();
    }

    /**
     * Förfrågan om uppkoppling. Utför uppkopplingsjobb under 2 ms.
     */
    private void arrival_a() {
        //Om ingen i kön så utför jobbet.
        if (numberInQueueA == 0 && numberInQueueB == 0)
            InsertEvent(READY_A, time + 0.002);               // konstant betjäningstid = 2 ms
        numberInQueueA++;
        InsertEvent(ARRIVAL_A, time + generateMean(1.0 / 150));          // λ = 150
    }

    /**
     * Färdigbetjänad - Koppling är gjord och ska vara i 1 s.
     */
    private void ready_a() {
        numberInQueueA--;
        //Om fler i kön, utför jobb, i första hand b
        //FIXME: Ordning på dessa två if:s: Om A kommer först så är den prioritet (senare uppgift). Om B är först så har B prio som i tidigare uppgifter i labben
        if (numberInQueueA > 0)
            InsertEvent(READY_A, time + 0.002);             // konstant betjäningstid = 2 ms
        else if (numberInQueueB > 0)
            InsertEvent(READY_B, time + 0.004);             // konstant betjäningstid = 4 ms
//        InsertEvent(ARRIVAL_B, time + 1);                   // koppling ska vara under 1 s (sen kopplas ner dvs arrival_b)
        InsertEvent(ARRIVAL_B, time + generateMean(1));     // koppling ska vara under 1 s i GENOMSNITT
    }


    /**
     * Förfrågan om nerkoppling (Kopplingstid över). Utför nerkopplingsjobb under 4 ms.
     */
    private void arrival_b() {
        if (numberInQueueA == 0 && numberInQueueB == 0)
            InsertEvent(READY_B, time + 0.004);     // konstant betjäningstid = 4 ms
        numberInQueueB++;
    }

    /**
     * Nerkoppling gjord. "Kund" helt betjänad.
     */
    private void ready_b() {
        numberInQueueB--;
        //FIXME: Ordning på dessa två if:s: Om A kommer först så är den prioritet (senare uppgift). Om B är först så har B prio som i tidigare uppgifter i labben
        if (numberInQueueA > 0)
            InsertEvent(READY_A, time + 0.002);     // konstant betjäningstid = 2 ms
        else if (numberInQueueB > 0)
            InsertEvent(READY_B, time + 0.004);     // konstant betjäningstid = 4 ms
        //skräp
//        if (numberInQueueB > 0)
//            InsertEvent(READY_B, time + 0.004);               //konstant betjäningstid = 4 ms
    }

    private void measure() {
        accumulated = accumulated + numberInQueueA + numberInQueueB;
        noMeasurements++;
        InsertEvent(MEASURE, time + 0.1);           // konstant tid mellan mätningar = 0.1 s
    }
}